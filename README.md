# cis2232_20192020_project_black
Team Black: CIS2232 Course Project

Contact: BJ MacLean bjmaclean@hollandcollege.com

Team members:
	Thomas Hickey | thickey2@hollandcollege.com
	Kahla Julius  | kjulius@hollandcollege.com
	Md Ahsanul Hoque | ahrony90@gmail.com
	
Overview
Welcome to the CIS2232 project.  It will require implementation of the basic outcomes of the course.  The project must work with the environment for the CIS development team.  The system code will adhere to CIS standards.  This includes naming and commenting conventions. The project should follow an object-oriented design with packages used to organize classes.  These are constraints put on the development based on the organizational standards.
Users will have a username and password which will be used to login to the system.  Their username is their email address.  The cisadmin application can administer the users of the system.  Login is  a requirement but administering the access is not a requirement that needs to be added.
Although most of the requirements are individual, teams must complete some together.  
•	The project manager (aka learning manager) will assign a color scheme to each team.
•	The developer will build the system according to Object-oriented design principles.   
•	The createDatabase.sql file will create the application database.  The learning manager must approve any changes to the database structure.  
•	The due dates for sprints are contained in the SAM calendar.