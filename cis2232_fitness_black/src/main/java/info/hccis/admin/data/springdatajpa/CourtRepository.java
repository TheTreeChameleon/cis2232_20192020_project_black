package info.hccis.admin.data.springdatajpa;

import info.hccis.admin.model.jpa.CodeType;
import info.hccis.admin.model.jpa.CodeValue;
import info.hccis.admin.model.jpa.Court;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @Purpose: Provides repo for Court entity
 * @since 20191031
 * @author Md Ahsanul Hoque
 */
@Repository
public interface CourtRepository extends CrudRepository<Court, Integer>{
    
}
