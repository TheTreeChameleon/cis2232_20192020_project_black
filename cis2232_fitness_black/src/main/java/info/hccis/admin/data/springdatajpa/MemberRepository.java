package info.hccis.admin.data.springdatajpa;

import info.hccis.admin.model.jpa.FitnessMember;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MemberRepository extends CrudRepository<FitnessMember, Integer> {


}