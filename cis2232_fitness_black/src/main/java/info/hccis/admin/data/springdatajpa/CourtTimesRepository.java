/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.data.springdatajpa;

import info.hccis.admin.model.jpa.Courttimes;
import java.io.Serializable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ahron
 */
@Repository
public interface  CourtTimesRepository extends CrudRepository<Courttimes, Serializable>{
    
}
