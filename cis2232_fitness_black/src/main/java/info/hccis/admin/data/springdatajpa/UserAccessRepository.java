package info.hccis.admin.data.springdatajpa;

import info.hccis.admin.model.jpa.UserAccess;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserAccessRepository extends CrudRepository<UserAccess, Integer> {


}