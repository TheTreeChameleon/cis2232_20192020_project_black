package info.hccis.admin.data.springdatajpa;

import info.hccis.admin.model.jpa.CourtBooking;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Kahla Julius
 * @since 30th October 2019
 * 
 * CourtBooking repository for court booking entity provided
 */

@Repository
public interface CourtBookingRepository extends CrudRepository<CourtBooking, Integer> {
    //List<CourtBooking> findByCourtType(int courtNumber);
}