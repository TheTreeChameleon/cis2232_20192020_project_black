package info.hccis.admin.web;

import com.google.gson.Gson;
import info.hccis.admin.data.springdatajpa.CourtBookingRepository;
import info.hccis.admin.data.springdatajpa.CourtTimesRepository;
import info.hccis.admin.data.springdatajpa.MemberRepository;
import info.hccis.admin.model.CourtBookingReport;
import info.hccis.admin.model.CourtReport;
import info.hccis.admin.model.jpa.CodeValue;
import info.hccis.admin.model.jpa.CourtBooking;
import info.hccis.admin.model.jpa.Courttimes;
import info.hccis.admin.model.jpa.FitnessMember;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author kjulius
 * @since 30th October 2019
 */
@Controller
public class CourtBookingController {

    private final CourtBookingRepository cbr;
    private final MemberRepository mr; 
    private final CourtTimesRepository ctr;
    
    @Autowired
    public CourtBookingController(CourtBookingRepository cbr, MemberRepository mr, CourtTimesRepository ctr) {
        this.cbr = cbr;
        this.mr = mr;
        this.ctr = ctr;
    }

    @RequestMapping("/courtBooking/save")
    public String saveUser(Model model, HttpSession session, HttpServletRequest request) throws IOException {
        Iterable<CourtBooking> courtBooking = cbr.findAll();

        //Get the current date and format it
        LocalDateTime ldt = LocalDateTime.now();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyLddhhmmss");
        String formattedDate = ldt.format(dtf);

        //Make the file paths and the file if it doesn't exist
        String path = "C:\\fitness\\";
        String file = path + "courts_" + formattedDate + ".json";
        Files.createDirectories(Paths.get(path));
        if (!Files.exists(Paths.get(file))) {
            Files.createFile(Paths.get(file));
        }

        //Write to the file
        BufferedWriter bw = new BufferedWriter(new FileWriter(file));
        Gson gson = new Gson();
        for (CourtBooking current : courtBooking) {
            bw.write(gson.toJson(current));
            bw.newLine();
        }
        bw.close();

        //Return the user to the website that tells them it was exported successfully
        return "courtBooking/success";
    }

    @RequestMapping("/courtBooking/list")
    public String listCourtBookings(Model model) {
        model.addAttribute("courtbooking", cbr.findAll());
        ArrayList<CourtBooking> theCourts = (ArrayList<CourtBooking>) cbr.findAll();
        System.out.println("TKH-There are " + theCourts.size() + " courts");
        return "courtBooking/list";
    }

    @RequestMapping("/courtBooking/book")
    public String addCourtBookings(Model model) {
        CourtBooking court = new CourtBooking();
        ArrayList<FitnessMember> members = (ArrayList<FitnessMember>) mr.findAll();
        model.addAttribute("courtbooking", court);
        model.addAttribute("members", members);
        return "courtBooking/book";
    }

    @RequestMapping("/courtBooking/bookSubmit")
    public String courtAddBookingSubmit(Model model, @Valid @ModelAttribute("court") CourtBooking theCourtFromTheForm, BindingResult result) {

        if (result.hasErrors()) {
            System.out.println("Error in validation.");
            String error = "Validation Error";
            model.addAttribute("message", error);
            return "courtBooking/book";
        }
        try {
            cbr.save(theCourtFromTheForm);
        } catch (Exception e) {
            System.out.println("Could not save to the database");
        }
        model.addAttribute("courtbooking", cbr.findAll());
        return "courtBooking/list";
    }

    @RequestMapping("/courtBooking/update")
    public String courtBookingUpdate(Model model, HttpServletRequest request) {

        //- This method will use the ID which is passed as a request 
        //parameter.  
        String idToFind = request.getParameter("id");
        System.out.println("TKH id passed=" + idToFind);
        //- It will go to the database and load the OJT Reflection details into an OJTReflection
        //  object and put that object in the model.  
        CourtBooking editCourt = cbr.findOne(Integer.parseInt(idToFind));

        model.addAttribute("courtbooking", editCourt);
        return "/courtBooking/book";

    }

    @RequestMapping("/courtBooking/delete")
    public String courtBookingDelete(Model model, HttpServletRequest request) {

        //- This method will use the ID which is passed as a request 
        //parameter.  
        String idToFind = request.getParameter("id");
        System.out.println("TKH id passed=" + idToFind);
        try {
            //- It will go to the database and load the OJTReflection details into an OJTReflection
            //  object and put that object in the model.
            cbr.delete(Integer.parseInt(idToFind));
        } catch (Exception ex) {
            System.out.println("Could not delete");
        }

        //Reload the OJTReflections list so it can be shown on the next view.
        model.addAttribute("courtbooking", cbr.findAll());
        return "/courtBooking/list";
    }
    
    @RequestMapping("/courtBooking/report")
    public String checkCourts(Model model){
        CourtBookingReport bookingReport = new CourtBookingReport();
        model.addAttribute("courtbooking", bookingReport);
        model.addAttribute("courtbooking", cbr.findAll());
        return "courtBooking/fitnessMemberReport";
    }
    
    @RequestMapping("/courtBooking/Search")
    public String checkCourtBookingSubmit(Model model, @Valid @ModelAttribute("courtBookingReport") CourtBookingReport courtBookingReportFromTheForm, BindingResult result){
        
        if (result.hasErrors()) {
            System.out.println("Error in validation.");
            String error = "Please enter the information...";
            model.addAttribute("message", error);
            CourtBookingReport bookingReport = new CourtBookingReport();
            model.addAttribute("courtBookingReport", bookingReport);
            ArrayList<CourtBooking> theCourts = (ArrayList<CourtBooking>) cbr.findAll();
            model.addAttribute("theCourts", theCourts);
            return "courtBooking/check";
        }
        
        //String message = "No data found";
        List<CourtBookingReport> courtBookingList = new ArrayList<>();;
        
        //List<CourtReport> report = availableCourtList.stream();
        CourtBookingReport reportObject;
        
        //Iterable<CourtBooking> courtBookingFromRepo = cbr.findByCourtType(courtReportFromTheForm.getCourtType());
        Iterable<CourtBooking> courtBookingFromRepo = cbr.findAll();
        
        Iterable<Courttimes> courtTimes = ctr.findAll();   
        
        //check if the date and time is already booked, if not show it on the report with other available court that are not yet booked
        if (isBooked(Integer.parseInt(courtBookingReportFromTheForm.getFitnessMembers()), Integer.parseInt(courtBookingReportFromTheForm.getFitnessMembers()))) {
            for (Courttimes ct : courtTimes) {
                reportObject = new CourtBookingReport();
                if(Integer.parseInt(courtBookingReportFromTheForm.getFitnessMembers()) != Integer.parseInt(ct.getStartTime())){
                    //reportObject.setCourtBookings(ct.getStartTime());
                    reportObject.setStartDate(courtBookingReportFromTheForm.getStartDate());
                    reportObject.setEndDate(courtBookingReportFromTheForm.getEndDate());
                    courtBookingList.add(reportObject);
                }
            }
        }else{
            for (Courttimes ct : courtTimes) {
                reportObject = new CourtBookingReport();
                reportObject.setFitnessMembers(ct.getStartTime());
                reportObject.setStartDate(courtBookingReportFromTheForm.getStartDate());
                reportObject.setEndDate(courtBookingReportFromTheForm.getEndDate());
                courtBookingList.add(reportObject);  
            }
        }
        model.addAttribute("courtBookingList", courtBookingList);   
        
        for(CourtBookingReport c: courtBookingList){
            System.out.println("Fitness Member: "+ c.getFitnessMembers()+ ", Court Booking: " + c.getCourtBookings()+ ", Start Date: " + c.getStartDate()+ ", End Date: " + c.getEndDate());
        }
        return "courtbooking/courtBookingReport";
    }
    public boolean isBooked(int date, int time){
        Iterable<CourtBooking> courtBookingFromRepo = cbr.findAll();
        boolean flag = false;
        for(CourtBooking current : courtBookingFromRepo){
            if ((date == Integer.parseInt(current.getBookingDate()))) {
                if (time == Integer.parseInt(current.getStartTime())) {
                    flag = true;
                    break;
                }
            }
        }
        return flag;
    }
}
