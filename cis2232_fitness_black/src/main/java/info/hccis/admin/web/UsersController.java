package info.hccis.admin.web;

import info.hccis.admin.dao.CodeValueDAO;
import info.hccis.admin.dao.UserDAO;
import info.hccis.admin.model.DatabaseConnection;
import info.hccis.admin.model.jpa.User;
import info.hccis.admin.service.CodeService;
import info.hccis.admin.util.Utility;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 */
@Controller
public class UsersController {

    private final CodeService codeService;

//    public UsersController(){
//        //default constructor for injection.
//    }
    @Autowired
    public UsersController(CodeService cs) {
        this.codeService = cs;
    }

    @RequestMapping("/users/list")
    public String showUsers(Model model, HttpSession session) {
        DatabaseConnection databaseConnection = (DatabaseConnection) session.getAttribute("db");
        ArrayList<User> users = UserDAO.getUsers(databaseConnection);
        model.addAttribute("users", users);
        return "users/list";
    }

    @RequestMapping("/users/add")
    public String addUser(Model model, HttpSession session, @ModelAttribute("user") User user) {
        //UserAccess userAccess = new UserAccess();
        DatabaseConnection databaseConnection = (DatabaseConnection) session.getAttribute("db");

        String result = "pass";
        if (user != null && user.getUsername() != null && !user.getUsername().isEmpty()) {
            //Setting Hashed password
            user.setPassword(Utility.getHashPassword(user.getPassword()));
            if (user.getUserId() == null) {
                user.setUserId(0);
                user.setCreatedDateTime(Utility.getNow(""));
            }
            try {

                UserDAO.update(databaseConnection, user);
            } catch (Exception e) {
                result = "fail";
                System.out.println("There was an error saving the user.");
                e.printStackTrace();
            }
            if (!result.equals("Fail")) {
                model.addAttribute("message", "User added");
                model.addAttribute("user", new User());

                //Load users into the session for drop down.
                ArrayList<User> users = UserDAO.getUsers(databaseConnection);
                for (User current : users) {
                    current.setUserTypeDescription(CodeValueDAO.getCodeValueDescription(databaseConnection, 1, current.getUserTypeCode()));
                }
                model.addAttribute("users", users);
                return "users/list";
            } else {
                model.addAttribute("message", "Failed to add user");
                model.addAttribute("user", user);
            }
        }

        return "users/add";

    }

    @RequestMapping("/users/delete")
    public String deleteUser(Model model, HttpSession session, HttpServletRequest request) {

        DatabaseConnection databaseConnection = (DatabaseConnection) session.getAttribute("db");
        try {
            UserDAO.delete(databaseConnection, Integer.parseInt(request.getParameter("id")));
        } catch (Exception ex) {
            System.out.println("BJM Error deleting user ");
            ex.printStackTrace();
        }

        //Load users into the session for drop down.
        ArrayList<User> users = UserDAO.getUsers(databaseConnection);
        for (User current : users) {
            current.setUserTypeDescription(CodeValueDAO.getCodeValueDescription(databaseConnection, 1, current.getUserTypeCode()));
        }
        model.addAttribute("users", users);

        return "users/list";
    }

}
