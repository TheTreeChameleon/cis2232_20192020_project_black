package info.hccis.admin.web;

import com.google.gson.Gson;
import info.hccis.admin.data.springdatajpa.CodeValueRepository;
import info.hccis.admin.data.springdatajpa.CourtBookingRepository;
import info.hccis.admin.data.springdatajpa.CourtRepository;
import info.hccis.admin.data.springdatajpa.CourtTimesRepository;
import info.hccis.admin.model.CourtReport;
import info.hccis.admin.model.jpa.CodeValue;
import info.hccis.admin.model.jpa.Court;
import info.hccis.admin.model.jpa.CourtBooking;
import info.hccis.admin.model.jpa.Courttimes;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import javax.validation.Valid;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @since 20191030
 * @author Md Ahsanul Hoque
 */
@Controller
public class CourtController {
    private final CourtRepository cr;
    private final CourtBookingRepository cbr;
    private final CodeValueRepository cvr;
    private final CourtTimesRepository ctr;
    @Autowired
    public CourtController(CourtRepository cr, CourtBookingRepository cbr, CodeValueRepository cvr, CourtTimesRepository ctr){
        this.cr = cr;
        this.cbr = cbr;
        this.cvr = cvr;
        this.ctr = ctr;
    }
    
    @RequestMapping("/courts/save")
    public String saveCourts(Model model, HttpSession session, HttpServletRequest request) throws IOException {
        Iterable<Court> courts = cr.findAll();
        //Return the court data to the website that tells them it was exported successfully
        //Get the current date and format it
        LocalDateTime ldt = LocalDateTime.now();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyLddhhmmss");
        String formattedDate = ldt.format(dtf);
        
        //Make the file paths and the file if it doesn't exist
        String path = "C:\\fitness\\";
        String file = path+"court_"+formattedDate+".json";
        
        Files.createDirectories(Paths.get(path));
        if (!Files.exists(Paths.get(file))) {
            Files.createFile(Paths.get(file));
        }
        
        //Write to the file
        BufferedWriter bw = new BufferedWriter(new FileWriter(file));
        Gson gson = new Gson();
        for (Court current: courts) {
            bw.write(gson.toJson(current));
            bw.newLine();
        }
        bw.close();
        
        return "courts/success";
    }
    
    @RequestMapping("/courts/list")
    public String listCourts(Model model) {
        model.addAttribute("court", cr.findAll());
        ArrayList<Court> theCourts = (ArrayList<Court>) cr.findAll();
        System.out.println("TKH-There are "+theCourts.size()+" courts");
        return "courts/list";
    }

    @RequestMapping("/courts/add")
    public String addCourt(Model model) {
        Court court = new Court();
        model.addAttribute("court",court);
        return "courts/add";
    }

    @RequestMapping("/courts/addSubmit")
    public String courtAddSubmit(Model model, @Valid @ModelAttribute("court") Court theCourtFromTheForm, BindingResult result) {

        if (result.hasErrors()) {
            System.out.println("Error in validation.");
            String error = "Validation Error";
            model.addAttribute("message", error);
            return "courts/add";
        }
        try {
            cr.save(theCourtFromTheForm);
        } catch (Exception e) {
            System.out.println("Could not save to the database");
        }

        model.addAttribute("court", cr.findAll());

        return "courts/list";
    }

    @RequestMapping("/courts/update")
    public String courtUpdate(Model model, HttpServletRequest request) {

        //- This method will use the ID which is passed as a request 
        //parameter.  
        String idToFind = request.getParameter("id");
        System.out.println("TKH id passed=" + idToFind);
        //- It will go to the database and load the OJT Reflection details into an OJTReflection
        //  object and put that object in the model.  
        Court editCourt = cr.findOne(Integer.parseInt(idToFind));

        model.addAttribute("court", editCourt);
        return "/courts/add";

    }

    @RequestMapping("/courts/delete")
    public String courtDelete(Model model, HttpServletRequest request) {

        //- This method will use the ID which is passed as a request 
        //parameter.  
        String idToFind = request.getParameter("id");
        System.out.println("TKH id passed=" + idToFind);
        try {
            //- It will go to the database and load the OJTReflection details into an OJTReflection
            //  object and put that object in the model.
            cr.delete(Integer.parseInt(idToFind));
        } catch (Exception ex) {
            System.out.println("Could not delete");
        }

        //Reload the OJTReflections list so it can be shown on the next view.
        model.addAttribute("court", cr.findAll());
        return "/courts/list";

    }
    /**
    * @since 20191210
    * @purpose: let the user to check for available courts for booking
    * @author Md Ahsanul Hoque
    */    
    @RequestMapping("/courts/check")
    public String checkCourts(Model model){
        CourtReport courtReport = new CourtReport();
        model.addAttribute("courtReport", courtReport);
        List<CodeValue> codeValues = cvr.findByCodeTypeId(2);//This is basically court type
        model.addAttribute("codeValues", codeValues);
        return "courts/check";
    }
     /**
    * @since 20191210
    * @purpose: list out available courts after checking
    * @author Md Ahsanul Hoque
    */   
    @RequestMapping("/courts/checkSubmit")
    public String checkCourtSubmit(Model model, @Valid @ModelAttribute("courtReport") CourtReport courtReportFromTheForm, BindingResult result){
        
        if (result.hasErrors()) {
            System.out.println("Error in validation.");
            String error = "Please entere the information...";
            model.addAttribute("message", error);
            CourtReport courtReport = new CourtReport();
            model.addAttribute("courtReport", courtReport);
            List<CodeValue> codeValues = cvr.findByCodeTypeId(2);//This is basically court type
            model.addAttribute("codeValues", codeValues);
            return "courts/check";
        }
        
        //String message = "No data found";
        
        List<CourtReport> availableCourtList = new ArrayList<>();;
        
        //List<CourtReport> report = availableCourtList.stream();
        
        CourtReport reportObject;
        
        //Iterable<CourtBooking> courtBookingFromRepo = cbr.findByCourtType(courtReportFromTheForm.getCourtType());
        Iterable<CourtBooking> courtBookingFromRepo = cbr.findAll();
        
        
        Iterable<Courttimes> courtTimes = ctr.findAll();
        
        
        //check if the date and time is already booked, if not show it on the report with other available court that are not yet booked
        if (isBooked(Integer.parseInt(courtReportFromTheForm.getDate()), Integer.parseInt(courtReportFromTheForm.getTime()))) {
            for (Courttimes ct : courtTimes) {
                reportObject = new CourtReport();
                if(Integer.parseInt(courtReportFromTheForm.getTime()) != Integer.parseInt(ct.getStartTime())){
                    reportObject.setTime(ct.getStartTime());
                    reportObject.setDate(courtReportFromTheForm.getDate());
                    reportObject.setCourtType(courtReportFromTheForm.getCourtType());
                    availableCourtList.add(reportObject);
                }
            }
        }else{
            for (Courttimes ct : courtTimes) {
                reportObject = new CourtReport();
                reportObject.setTime(ct.getStartTime());
                reportObject.setDate(courtReportFromTheForm.getDate());
                reportObject.setCourtType(courtReportFromTheForm.getCourtType());
                availableCourtList.add(reportObject);
                
            }
        }
        //availableCourtList.clear();
        model.addAttribute("availableCourtList", availableCourtList);   
        
        for(CourtReport c: availableCourtList){
            System.out.println("Court type: "+ c.getCourtType() + ", Date: " + c.getDate() + ", Time: " + c.getTime());
        }
        return "courts/courtsReport";
    }
    
    /**
    * @since 20191210
    * @purpose: checking whether the time is already booked or not
    * @author Md Ahsanul Hoque
    */
    public boolean isBooked(int date, int time){
        Iterable<CourtBooking> courtBookingFromRepo = cbr.findAll();
        boolean flag = false;
        for(CourtBooking current : courtBookingFromRepo){
            if ((date == Integer.parseInt(current.getBookingDate()))) {
                if (time == Integer.parseInt(current.getStartTime())) {
                    flag = true;
                    break;
                }
            }
        }
        return flag;
    }
}
