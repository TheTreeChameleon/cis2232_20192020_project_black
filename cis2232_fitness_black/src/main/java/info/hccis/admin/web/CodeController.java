package info.hccis.admin.web;

import info.hccis.admin.dao.CodeTypeDAO;
import info.hccis.admin.dao.CodeValueDAO;
import info.hccis.admin.data.springdatajpa.CodeTypeRepository;
import info.hccis.admin.data.springdatajpa.CodeValueRepository;
import info.hccis.admin.model.DatabaseConnection;
import info.hccis.admin.model.jpa.CodeType;
import info.hccis.admin.model.jpa.CodeValue;
import info.hccis.admin.service.CodeService;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CodeController {

    private final CodeService codeService;
//    private final CodeValueRepository cvr;
//    private final CodeTypeRepository ctr;

    @Autowired
    public CodeController(CodeService codeService, CodeValueRepository cvr, CodeTypeRepository ctr) {
        this.codeService = codeService;
//        this.cvr = cvr;
//        this.ctr = ctr;
    }

    @RequestMapping("/codes/databaseSetup")
    public String enter(Model model, HttpSession session, DatabaseConnection dbConnectionIn) {
        System.out.println("Database provided=" + dbConnectionIn.getDatabaseName());
        model.addAttribute("db", dbConnectionIn);
        session.setAttribute("db", dbConnectionIn);
        ArrayList<CodeType> codes = codeService.getCodeTypes(dbConnectionIn);
        model.addAttribute("codeTypes", codes);
        ArrayList<CodeValue> UserTypes = (ArrayList<CodeValue>) CodeValueDAO.getCodeValues(dbConnectionIn, "1");
        session.setAttribute("UserTypes", UserTypes);
        return "/codes/codeTypes";
    }

    @RequestMapping("/codes/codeTypes")
    public String showCodes(Model model, HttpSession session) {

        DatabaseConnection databaseConnection = (DatabaseConnection) session.getAttribute("db");
        ArrayList<CodeType> codes = codeService.getCodeTypes(databaseConnection);
        model.addAttribute("codeTypes", codes);
        return "/codes/codeTypes";
    }

    @RequestMapping("/codes/codeValues")
    public String showCodeValues(Model model, HttpServletRequest request) {
        String id = request.getParameter("id");
        System.out.println("id passed in to controller is:" + id);
        model.addAttribute("codeTypeId", id);
        //Get the code values 
        DatabaseConnection dbConnection = (DatabaseConnection) request.getSession().getAttribute("db");
        ArrayList<CodeValue> theList = CodeValueDAO.getCodeValues(dbConnection, id);
        model.addAttribute("codeValues", theList);
        System.out.println("BJM found some codeValues (" + theList.size() + ")");

        return "/codes/codeValues";
    }

    @RequestMapping("/codes/codeTypeAdd")
    public String codeTypeAdd(Model model, HttpServletRequest request) {
        DatabaseConnection dbConnection = (DatabaseConnection) request.getSession().getAttribute("db");
        System.out.println("BJM in codeTypeAdd");
        CodeType ct = new CodeType();
        model.addAttribute("codeType", ct);
        return "/codes/codeTypeEdit";
    }

    @RequestMapping("/codes/codeValueAdd")
    public String codeValueAdd(Model model, HttpServletRequest request) {
        DatabaseConnection dbConnection = (DatabaseConnection) request.getSession().getAttribute("db");
        String id = request.getParameter("id");
        System.out.println("BJM in codeValueAdd, id passed in to controller is:" + id);
        CodeValue cv = new CodeValue();
        cv.setCodeTypeId(Integer.parseInt(id));
        model.addAttribute("codeValue", cv);
        return "/codes/codeValueEdit";
    }

    @RequestMapping("/codes/codeValueEdit")
    public String codeValueEdit(Model model, HttpServletRequest request) {
        DatabaseConnection dbConnection = (DatabaseConnection) request.getSession().getAttribute("db");
        String id = request.getParameter("id");
        String sequence = request.getParameter("sequence");
        System.out.println("BJM in codeValueEdit, id passed in to controller is:" + id);
        CodeValue cv = CodeValueDAO.getCodeValue(dbConnection, id, sequence);
        model.addAttribute("codeValue", cv);
        return "/codes/codeValueEdit";
    }

    @RequestMapping("/codes/codeValueEditSubmit")
    public String codeValueEditSubmit(Model model, HttpServletRequest request, @Valid @ModelAttribute("codeValue") CodeValue codeValue, BindingResult result) {


        System.out.println("Made it to code value edit submit, description=" + codeValue.getEnglishDescription());
        if (result.hasErrors()) {
            System.out.println("Error in validation of code value.");
            return "/codes/codeValueEdit";
        }
        DatabaseConnection dbConnection = (DatabaseConnection) request.getSession().getAttribute("db");
        CodeValueDAO.addUpdate(dbConnection, codeValue);

        //next send them back to the code values for the code type.
        ArrayList<CodeValue> theList = CodeValueDAO.getCodeValues(dbConnection, String.valueOf(codeValue.getCodeTypeId()));
//        //use jpa
//        ArrayList<CodeValue> theList = (ArrayList<CodeValue>) cvr.findByCodeTypeId(codeValue.getCodeTypeId());
        model.addAttribute("codeValues", theList);

        model.addAttribute("codeTypeId", codeValue.getCodeTypeId());
        //send to the codeValues view.
        return "/codes/codeValues";
    }

    @RequestMapping("/codes/codeTypeEditSubmit")
    public String codeTypeEditSubmit(Model model, HttpServletRequest request, @Valid @ModelAttribute("codeType") CodeType codeType, BindingResult result) {

        System.out.println("Made it to code type edit submit, description=" + codeType.getEnglishDescription());
        if (result.hasErrors()) {
            System.out.println("Error in validation of code type.");
            return "/codes/codeTypeEdit";
        }
        DatabaseConnection dbConnection = (DatabaseConnection) request.getSession().getAttribute("db");
        CodeTypeDAO.add(dbConnection, codeType, "ADMIN");
        //use jpa...no since using database connection provided on page.
//        ctr.save(codeType);

        //use jpa
        //ArrayList<CodeType> theList = (ArrayList<CodeType>) ctr.findAll();
        ArrayList<CodeType> theList = CodeTypeDAO.getCodeTypes(dbConnection);
        model.addAttribute("codeTypes", theList);

        //send to the codeTypes view.
        return "/codes/codeTypes";
    }

}
