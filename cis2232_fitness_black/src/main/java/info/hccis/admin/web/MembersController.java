package info.hccis.admin.web;

import com.google.gson.Gson;
import info.hccis.admin.data.springdatajpa.CourtBookingRepository;
import info.hccis.admin.model.MemberBooking;
import info.hccis.admin.model.jpa.CourtBooking;
import info.hccis.admin.data.springdatajpa.CodeValueRepository;
import info.hccis.admin.data.springdatajpa.MemberRepository;
import info.hccis.admin.data.springdatajpa.UserAccessRepository;
import info.hccis.admin.model.jpa.CodeValue;
import info.hccis.admin.model.jpa.FitnessMember;
import info.hccis.admin.model.jpa.UserAccess;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 */
@Controller
public class MembersController {

    private final MemberRepository mr;
    private final CourtBookingRepository cbr;
    private final CodeValueRepository cvr;
    private final UserAccessRepository uar;


    /**
     * Default constructor for the MembersController
     * @author Thomas Hickey
     * @since 2019
     * @param mr
     * @param cbr
     * @param cvr
     * @param uar 
     */
    @Autowired
    public MembersController(MemberRepository mr, CourtBookingRepository cbr, CodeValueRepository cvr, UserAccessRepository uar) {
        this.mr = mr;
        this.cbr = cbr;
        this.cvr = cvr;
        this.uar = uar;
    }

    /**
     * Method to save the members to a file
     * @author Thomas Hickey
     * @since 2019
     * @param model
     * @param session
     * @param request
     * @return
     * @throws IOException 
     */
    @RequestMapping("/members/save")
    public String saveUser(Model model, HttpSession session, HttpServletRequest request) throws IOException {
        
        Iterable<FitnessMember> members = mr.findAll();
        
        //Get the current date and format it
        LocalDateTime ldt = LocalDateTime.now();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyLddhhmmss");
        String formattedDate = ldt.format(dtf);
        
        //Make the file paths and the file if it doesn't exist
        String path = "C:\\fitness\\";
        String file = path+"member_"+formattedDate+".json";
        Files.createDirectories(Paths.get(path));
        if (!Files.exists(Paths.get(file))) {
            Files.createFile(Paths.get(file));
        }
        
        //Write to the file
        BufferedWriter bw = new BufferedWriter(new FileWriter(file));
        Gson gson = new Gson();
        for (FitnessMember current: members) {
            bw.write(gson.toJson(current));
            bw.newLine();
        }
        bw.close();
        
        //Return the user to the website that tells them it was exported successfully
        return "members/success";
    }
    
    /**
     * Method to send the user to a page to view a member's bookings
     * @author Thomas Hickey
     * @since 2019
     * @param model
     * @return 
     */
    @RequestMapping("/members/memberBooking")
    public String findBookings(Model model) {
        MemberBooking mb = new MemberBooking();
        ArrayList<UserAccess> members = (ArrayList<UserAccess>) uar.findAll();
        model.addAttribute("members", members);
        model.addAttribute("memberbooking", mb);
        return "members/memberBooking";
    }
    
    /**
     * Method to search through the members to find a match for the user's search results
     * @author Thomas Hickey
     * @since 2019
     * @param model
     * @param theMemberBookFromForm
     * @param result
     * @return 
     */
    @RequestMapping("/members/search")
    public String searchBookings(Model model, @Valid @ModelAttribute("memberbooking") MemberBooking theMemberBookFromForm, BindingResult result) {

        //Check for errors in validation (spring validation)
        if (result.hasErrors()) {
            System.out.println("Error in validation.");
            String error = "Validation Error";
            ArrayList<UserAccess> members = (ArrayList<UserAccess>) uar.findAll();
            model.addAttribute("message", error);
            model.addAttribute("members", members);
            model.addAttribute("memberbooking", theMemberBookFromForm);
            return "members/memberBooking";
        }
        
        //Create a list of all court booking
        ArrayList<CourtBooking> allCourts = (ArrayList<CourtBooking>) cbr.findAll();
        
        //Create a new blank list of court booking results
        ArrayList<CourtBooking> selectedCourts = new ArrayList();
        
        //Check in the full array list to see if there is a match to the court booking.
        //If there is, add the current courtbooking to the new blank array list
        for (CourtBooking current: allCourts) {
            int courtStartDate = Integer.parseInt(theMemberBookFromForm.getStartDate());
            int courtEndDate = Integer.parseInt(theMemberBookFromForm.getEndDate());
            int currentDate = Integer.parseInt(current.getBookingDate());
            int memberId = theMemberBookFromForm.getMemberId();
            int currentMemberId = current.getMemberId();
            if ((courtStartDate <= currentDate) && (currentDate <= courtEndDate)) {
                if (memberId == currentMemberId) {
                    selectedCourts.add(current);
                }
            }
        }

        //Put the selected courts into the model
        model.addAttribute("courts", selectedCourts);

        //Return them to the search page to display their results
        return "members/search";
    }
    
    /**
     * Method to get all members and return the user to the main list of members
     * @author Thomas Hickey
     * @since 2019
     * @param model
     * @param session
     * @param request
     * @return 
     */
    @RequestMapping("/members/list")
    public String list(Model model, HttpSession session, HttpServletRequest request){
        Iterable<FitnessMember> members = mr.findAll();
        
        model.addAttribute("members", members);
        model.addAttribute("codeValues", cvr.findAll());
        
        return "members/list";
    }
    
    /**
     * Method to take the user to the add page (to add a new member)
     * @author Thomas Hickey
     * @since 2019
     * @param model
     * @return 
     */
    @RequestMapping("/members/add")
    public String addMember(Model model){
        FitnessMember newMember = new FitnessMember();
        model.addAttribute("newMember", newMember);
        model.addAttribute("users", uar.findAll());
        model.addAttribute("codeValues", (Iterable<CodeValue>)cvr.findByCodeTypeId(3));
        
        return "members/add";
    }
    
    /**
     * Method to handle the user attempting to submit a new member (or edited member)
     * @author Thomas Hickey
     * @since 2019
     * @param model
     * @param memberFromForm
     * @param result
     * @param session
     * @return 
     */
    @RequestMapping("/members/addSubmit")
    public String addMemberSubmit(Model model, @Valid @ModelAttribute("newMember") FitnessMember memberFromForm, BindingResult result, HttpSession session){
        
        //If there is spring validation
        if (result.hasErrors()) {
            System.out.println("Error in validation");
            String error = "Validation failed";
            model.addAttribute("message", error);
            model.addAttribute("users", uar.findAll());
            model.addAttribute("codeValues", (Iterable<CodeValue>)cvr.findByCodeTypeId(3));
            return "members/add";
        }
        
        //Try to add the new member to the database
        try{
            info.hccis.admin.model.jpa.FitnessMember jpaFitnessMember = new info.hccis.admin.model.jpa.FitnessMember();
            
            jpaFitnessMember.setId(memberFromForm.getId());
            jpaFitnessMember.setPhoneCell(memberFromForm.getPhoneCell());
            jpaFitnessMember.setPhoneHome(memberFromForm.getPhoneHome());
            jpaFitnessMember.setPhoneWork(memberFromForm.getPhoneWork());
            jpaFitnessMember.setAddress(memberFromForm.getAddress());
            jpaFitnessMember.setStatus(memberFromForm.getStatus());
            jpaFitnessMember.setMembershipStartDate(memberFromForm.getMembershipStartDate());
            jpaFitnessMember.setMembershipEndDate(memberFromForm.getMembershipEndDate());
            jpaFitnessMember.setUserId(memberFromForm.getUserId());
            
            mr.save(jpaFitnessMember);
            
        }catch(Exception ex){
            Logger.getLogger(MembersController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //Send the user back to the listing page.
        Iterable<FitnessMember> members = mr.findAll();
        
        model.addAttribute("members", members);
        model.addAttribute("codeValues", cvr.findAll());
        
        return "members/list";
    }
    
    /**
     * Method to send the user to update a member
     * @author Thomas Hickey
     * @since 2019
     * @param model
     * @param request
     * @return 
     */
    @RequestMapping("/members/update")
    public String updateMember(Model model, HttpServletRequest request){
        info.hccis.admin.model.jpa.FitnessMember existingMember = mr.findOne(Integer.parseInt(request.getParameter("id")));
        model.addAttribute("newMember", existingMember);
        model.addAttribute("users", uar.findAll());
        model.addAttribute("codeValues", (Iterable<CodeValue>)cvr.findByCodeTypeId(3));
        
        return "members/add";
    }
    
    /**
     * Method to delete a member from the database
     * @author Thomas Hickey
     * @since 2019
     * @param model
     * @param request
     * @return 
     */
    @RequestMapping("/members/delete")
    public String delete(Model model, HttpServletRequest request){
        mr.delete(Integer.parseInt(request.getParameter("id")));
        model.addAttribute("members", mr.findAll());
        model.addAttribute("codeValues", cvr.findAll());
        return "members/list";
    }

}
