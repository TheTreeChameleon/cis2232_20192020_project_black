
package info.hccis.admin.rest;

import com.google.gson.Gson;
import info.hccis.admin.data.springdatajpa.CourtRepository;
import info.hccis.admin.model.jpa.Court;
import java.io.IOException;
import java.util.ArrayList;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 *
 * @author ahron
 */

@Path("/CourtService")
public class CourtRest {
    @Resource
    private final CourtRepository cr;
        /**
     * Note that dependency injection (constructor) is used here to provide the
     * MemberRepository object for use in this class.
     *
     * @param servletContext Context
     * @since 20181109
     * @author BJM Modified by MD
     */
    public CourtRest(@Context ServletContext servletContext){
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        this.cr = applicationContext.getBean(CourtRepository.class);
    }
    
    @GET
    @Path("/court/{param}")
    public Response getCourt(@PathParam("param") String id){
        String courtJson = "test" + id;
        Court court = cr.findOne(Integer.parseInt(id));
        if (court == null) {
            return Response.status(204).entity("{}").build();
        }else{
            Gson gson = new Gson();
            return Response.status(200).entity(gson.toJson(court)).build();
        }
    }
    /**
     * Service to provide all fitness members
     *
     * @author Md Ahsanul Hoque
     * @since December 06, 2019
     */
    @GET
    @Path("/court")
    public Response getCourts(){
        ArrayList<Court> courts = (ArrayList<Court>) cr.findAll();
        
        if (courts == null) {
            return Response.status(204).entity("[]").build();
        }else{
            Gson gson = new Gson();
            return Response.status(200).entity(gson.toJson(courts)).build();
        }
    }
    
    /**
    * POST operation which will add a court.
    *
    * @param jsonIn
    * @return response including the json representing the new fitness member.
    * @throws IOException
    */
    @POST
    @Path("/court/add")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addCourt(String jsonIn) throws IOException{
        Gson gson = new Gson();
        Court court = gson.fromJson(jsonIn, Court.class);
        
        court = cr.save(court);
        String temp = "";
        temp = gson.toJson(court);
        return Response.status(201).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();
    }
}
