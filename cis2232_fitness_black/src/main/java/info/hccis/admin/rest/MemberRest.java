package info.hccis.admin.rest;

import com.google.gson.Gson;
import info.hccis.admin.data.springdatajpa.MemberRepository;
import info.hccis.admin.model.jpa.FitnessMember;
import java.io.IOException;
import java.util.ArrayList;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * Services related to fitness members
 *
 * @author bjmaclean
 * @since Nov 6, 2019
 */
@Path("/FitnessMemberService")
public class MemberRest {

    @Resource
    private final MemberRepository mr;

    /**
     * Note that dependency injection (constructor) is used here to provide the
     * MemberRepository object for use in this class.
     *
     * @param servletContext Context
     * @since 20181109
     * @author BJM
     */
    public MemberRest(@Context ServletContext servletContext) {
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        this.mr = applicationContext.getBean(MemberRepository.class);
    }

    @GET
    @Path("fitnessmember/{param}")
    public Response getMember(@PathParam("param") String id) {

        String memberJson = "test" + id;

        FitnessMember member = mr.findOne(Integer.parseInt(id));

        if (member == null) {
            return Response.status(204).entity("{}").build();
        } else {
            Gson gson = new Gson();
            return Response.status(200).entity(gson.toJson(member)).build();
        }

    }

    /**
     * Service to provide all fitness members
     *
     * @author bjmaclean
     * @since Nov 6, 2019
     */
    @GET
    @Path("/fitnessmember")
    public Response getMembers() {

        ArrayList<FitnessMember> members = (ArrayList<FitnessMember>) mr.findAll();

        if (members == null) {
            return Response.status(204).entity("[]").build();
        } else {
            Gson gson = new Gson();
            return Response.status(200).entity(gson.toJson(members)).build();
        }

    }

    /**
     * POST operation which will add a fitness member.
     *
     * @param jsonIn
     * @return response including the json representing the new fitness member.
     * @throws IOException
     */
    @POST
    @Path("/fitnessmember/add")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addMember(String jsonIn) throws IOException {
        
        Gson gson = new Gson();
        FitnessMember member = gson.fromJson(jsonIn, FitnessMember.class);
        
        member = mr.save(member);
        String temp = gson.toJson(member);

        return Response.status(201).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();

    }
    
}

