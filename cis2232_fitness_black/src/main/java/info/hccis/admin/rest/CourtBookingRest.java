package info.hccis.admin.rest;

import com.google.gson.Gson;
import info.hccis.admin.data.springdatajpa.CourtBookingRepository;
import info.hccis.admin.model.jpa.CourtBooking;
import java.io.IOException;
import java.util.ArrayList;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * Services related to CourtBooking
 *
 * @author Kahla Julius
 * @since 4 December, 2019
 */
@Path("/BookingService")
public class CourtBookingRest {

    @Resource
    private final CourtBookingRepository cbr;

    /**
     * Note that dependency injection (constructor) is used here to provide the
     * CourtBookingRepository object for use in this class.
     *
     * @param servletContext Context
     * @since 4 December, 2019
     * @author Kahla Julius
     */
    public CourtBookingRest(@Context ServletContext servletContext) {
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        this.cbr = applicationContext.getBean(CourtBookingRepository.class);
    }

    @GET
    @Path("booking/{param}")
    public Response getCourtBooking(@PathParam("param") String id) {

        String memberJson = "test" + id;

        CourtBooking booking = cbr.findOne(Integer.parseInt(id));

        if (booking == null) {
            return Response.status(204).entity("{}").build();
        } else {
            Gson gson = new Gson();
            return Response.status(200).entity(gson.toJson(booking)).build();
        }
    }

    /**
     * Service to provide all CourtBooking
     *
     * @author Kahla Julius
     * @since 4 December, 2019
     */
    @GET
    @Path("/booking")
    public Response getCourtBooking() {

        ArrayList<CourtBooking> members = (ArrayList<CourtBooking>) cbr.findAll();

        if (members == null) {
            return Response.status(204).entity("[]").build();
        } else {
            Gson gson = new Gson();
            return Response.status(200).entity(gson.toJson(members)).build();
        }
    }

    /**
     * POST operation which will add a booking.
     *
     * @param jsonIn
     * @return response including the json representing the new fitness member.
     * @throws IOException
     * 
     * @author Kahla Julius
     * @since 4 December, 2019
     */
    @POST
    @Path("/booking/add")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addMember(String jsonIn) throws IOException {
        
        Gson gson = new Gson();
        CourtBooking booking = gson.fromJson(jsonIn, CourtBooking.class);
        
        booking = cbr.save(booking);
        String temp = gson.toJson(booking);

        return Response.status(201).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();
    }  
}

