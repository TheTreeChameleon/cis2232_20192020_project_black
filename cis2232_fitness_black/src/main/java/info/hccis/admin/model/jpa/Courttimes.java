/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.model.jpa;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author mhoque
 */
@Entity
@Table(name = "courttimes", catalog = "cis2232_fitness", schema = "")
@NamedQueries({
    @NamedQuery(name = "Courttimes.findAll", query = "SELECT c FROM Courttimes c"),
    @NamedQuery(name = "Courttimes.findByStartTime", query = "SELECT c FROM Courttimes c WHERE c.startTime = :startTime"),
    @NamedQuery(name = "Courttimes.findByEndTime", query = "SELECT c FROM Courttimes c WHERE c.endTime = :endTime")})
public class Courttimes implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "startTime", nullable = false, length = 5)
    private String startTime;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "endTime", nullable = false, length = 5)
    private String endTime;

    public Courttimes() {
    }

    public Courttimes(String startTime) {
        this.startTime = startTime;
    }

    public Courttimes(String startTime, String endTime) {
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (startTime != null ? startTime.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Courttimes)) {
            return false;
        }
        Courttimes other = (Courttimes) object;
        if ((this.startTime == null && other.startTime != null) || (this.startTime != null && !this.startTime.equals(other.startTime))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "info.hccis.admin.model.jpa.Courttimes[ startTime=" + startTime + " ]";
    }
    
}
