/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.model.jpa;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author thickey2
 */
@Entity
@Table(name = "court")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Court.findAll", query = "SELECT c FROM Court c"),
    @NamedQuery(name = "Court.findById", query = "SELECT c FROM Court c WHERE c.id = :id"),
    @NamedQuery(name = "Court.findByCourtNumber", query = "SELECT c FROM Court c WHERE c.courtNumber = :courtNumber"),
    @NamedQuery(name = "Court.findByCourtName", query = "SELECT c FROM Court c WHERE c.courtName = :courtName"),
    @NamedQuery(name = "Court.findByCourtType", query = "SELECT c FROM Court c WHERE c.courtType = :courtType")})
public class Court implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "courtNumber")
    private int courtNumber;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "courtName")
    private String courtName;
    @Basic(optional = false)
    @NotNull
    @Column(name = "courtType")
    private int courtType;

    public Court() {
    }

    public Court(Integer id) {
        this.id = id;
    }

    public Court(Integer id, int courtNumber, String courtName, int courtType) {
        this.id = id;
        this.courtNumber = courtNumber;
        this.courtName = courtName;
        this.courtType = courtType;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getCourtNumber() {
        return courtNumber;
    }

    public void setCourtNumber(int courtNumber) {
        this.courtNumber = courtNumber;
    }

    public String getCourtName() {
        return courtName;
    }

    public void setCourtName(String courtName) {
        this.courtName = courtName;
    }

    public int getCourtType() {
        return courtType;
    }

    public void setCourtType(int courtType) {
        this.courtType = courtType;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Court)) {
            return false;
        }
        Court other = (Court) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "info.hccis.admin.model.jpa.Court[ id=" + id + " ]";
    }
    
}
