/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.model.jpa;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author thickey2
 */
@Entity
@Table(name = "fitnessmember")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FitnessMember.findAll", query = "SELECT f FROM FitnessMember f"),
    @NamedQuery(name = "FitnessMember.findById", query = "SELECT f FROM FitnessMember f WHERE f.id = :id"),
    @NamedQuery(name = "FitnessMember.findByUserId", query = "SELECT f FROM FitnessMember f WHERE f.userId = :userId"),
    @NamedQuery(name = "FitnessMember.findByPhoneCell", query = "SELECT f FROM FitnessMember f WHERE f.phoneCell = :phoneCell"),
    @NamedQuery(name = "FitnessMember.findByPhoneHome", query = "SELECT f FROM FitnessMember f WHERE f.phoneHome = :phoneHome"),
    @NamedQuery(name = "FitnessMember.findByPhoneWork", query = "SELECT f FROM FitnessMember f WHERE f.phoneWork = :phoneWork"),
    @NamedQuery(name = "FitnessMember.findByAddress", query = "SELECT f FROM FitnessMember f WHERE f.address = :address"),
    @NamedQuery(name = "FitnessMember.findByStatus", query = "SELECT f FROM FitnessMember f WHERE f.status = :status"),
    @NamedQuery(name = "FitnessMember.findByMembershipStartDate", query = "SELECT f FROM FitnessMember f WHERE f.membershipStartDate = :membershipStartDate"),
    @NamedQuery(name = "FitnessMember.findByMembershipEndDate", query = "SELECT f FROM FitnessMember f WHERE f.membershipEndDate = :membershipEndDate")})
public class FitnessMember implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "userId")
    private int userId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "phoneCell")
    private String phoneCell;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "phoneHome")
    private String phoneHome;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "phoneWork")
    private String phoneWork;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "address")
    private String address;
    @Basic(optional = false)
    @NotNull
    @Column(name = "status")
    private int status;
    @Size(min = 8, max = 8)
    @Column(name = "membershipStartDate")
    private String membershipStartDate;
    @Size(min = 8, max = 8)
    @Column(name = "membershipEndDate")
    private String membershipEndDate;

    public FitnessMember() {
    }

    public FitnessMember(Integer id) {
        this.id = id;
    }

    public FitnessMember(Integer id, int userId, String phoneCell, String phoneHome, String phoneWork, String address, int status) {
        this.id = id;
        this.userId = userId;
        this.phoneCell = phoneCell;
        this.phoneHome = phoneHome;
        this.phoneWork = phoneWork;
        this.address = address;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getPhoneCell() {
        return phoneCell;
    }

    public void setPhoneCell(String phoneCell) {
        this.phoneCell = phoneCell;
    }

    public String getPhoneHome() {
        return phoneHome;
    }

    public void setPhoneHome(String phoneHome) {
        this.phoneHome = phoneHome;
    }

    public String getPhoneWork() {
        return phoneWork;
    }

    public void setPhoneWork(String phoneWork) {
        this.phoneWork = phoneWork;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMembershipStartDate() {
        return membershipStartDate;
    }

    public void setMembershipStartDate(String membershipStartDate) {
        this.membershipStartDate = membershipStartDate;
    }

    public String getMembershipEndDate() {
        return membershipEndDate;
    }

    public void setMembershipEndDate(String membershipEndDate) {
        this.membershipEndDate = membershipEndDate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FitnessMember)) {
            return false;
        }
        FitnessMember other = (FitnessMember) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "info.hccis.admin.model.jpa.FitnessMember[ id=" + id + " ]";
    }
    
}
