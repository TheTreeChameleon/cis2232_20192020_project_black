/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author ahron
 */
public class CourtReport {
    
    private int id;
    
    @NotNull
    @Size(min = 1, max = 8)
    private String date;
    
    @NotNull
    @Size(min = 1, max = 4)
    private String time;
    
    private int courtType;
    
    public CourtReport(){}
    
    public CourtReport(String date, String time, int codeType){
        this.date = date;
        this.time = time;
        this.courtType = codeType;
    }
    
    public int getId(){
        return id;
    }
    
    public void setId(int id){
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getCourtType() {
        return courtType;
    }

    public void setCourtType(int courtType) {
        this.courtType = courtType;
    }
}
