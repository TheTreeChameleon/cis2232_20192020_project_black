package info.hccis.admin.model;

/**
 *
 * @author Kahla Julius
 */
public class CourtBookingReport {
    private String fitnessMembers;
    private int courtBookings;
    private String startDate;
    private String endDate;
    
    public CourtBookingReport(){}
    
    public CourtBookingReport(String fitnessMembers, int courtBookings, String startDate, String endDate){
        this.fitnessMembers = fitnessMembers;
        this.courtBookings = courtBookings;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public String getFitnessMembers() {
        return fitnessMembers;
    }

    public void setFitnessMembers(String fitnessMembers) {
        this.fitnessMembers = fitnessMembers;
    }

    public int getCourtBookings() {
        return courtBookings;
    }

    public void setCourtBookings(int courtBookings) {
        this.courtBookings = courtBookings;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }
    
    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
