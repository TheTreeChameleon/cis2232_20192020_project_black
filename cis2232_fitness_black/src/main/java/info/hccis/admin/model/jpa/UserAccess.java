/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.model.jpa;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author thickey2
 */
@Entity
@Table(name = "useraccess")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserAccess.findAll", query = "SELECT u FROM UserAccess u"),
    @NamedQuery(name = "UserAccess.findByUserId", query = "SELECT u FROM UserAccess u WHERE u.userId = :userId"),
    @NamedQuery(name = "UserAccess.findByUsername", query = "SELECT u FROM UserAccess u WHERE u.username = :username"),
    @NamedQuery(name = "UserAccess.findByPassword", query = "SELECT u FROM UserAccess u WHERE u.password = :password"),
    @NamedQuery(name = "UserAccess.findByLastName", query = "SELECT u FROM UserAccess u WHERE u.lastName = :lastName"),
    @NamedQuery(name = "UserAccess.findByFirstName", query = "SELECT u FROM UserAccess u WHERE u.firstName = :firstName"),
    @NamedQuery(name = "UserAccess.findByUserTypeCode", query = "SELECT u FROM UserAccess u WHERE u.userTypeCode = :userTypeCode"),
    @NamedQuery(name = "UserAccess.findByAdditional1", query = "SELECT u FROM UserAccess u WHERE u.additional1 = :additional1"),
    @NamedQuery(name = "UserAccess.findByAdditional2", query = "SELECT u FROM UserAccess u WHERE u.additional2 = :additional2"),
    @NamedQuery(name = "UserAccess.findByCreatedDateTime", query = "SELECT u FROM UserAccess u WHERE u.createdDateTime = :createdDateTime")})
public class UserAccess implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "userId")
    private Integer userId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "username")
    private String username;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 128)
    @Column(name = "password")
    private String password;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "lastName")
    private String lastName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "firstName")
    private String firstName;
    @Basic(optional = false)
    @NotNull
    @Column(name = "userTypeCode")
    private int userTypeCode;
    @Size(max = 100)
    @Column(name = "additional1")
    private String additional1;
    @Size(max = 100)
    @Column(name = "additional2")
    private String additional2;
    @Size(max = 20)
    @Column(name = "createdDateTime")
    private String createdDateTime;

    public UserAccess() {
    }

    public UserAccess(Integer userId) {
        this.userId = userId;
    }

    public UserAccess(Integer userId, String username, String password, String lastName, String firstName, int userTypeCode) {
        this.userId = userId;
        this.username = username;
        this.password = password;
        this.lastName = lastName;
        this.firstName = firstName;
        this.userTypeCode = userTypeCode;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public int getUserTypeCode() {
        return userTypeCode;
    }

    public void setUserTypeCode(int userTypeCode) {
        this.userTypeCode = userTypeCode;
    }

    public String getAdditional1() {
        return additional1;
    }

    public void setAdditional1(String additional1) {
        this.additional1 = additional1;
    }

    public String getAdditional2() {
        return additional2;
    }

    public void setAdditional2(String additional2) {
        this.additional2 = additional2;
    }

    public String getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(String createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userId != null ? userId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserAccess)) {
            return false;
        }
        UserAccess other = (UserAccess) object;
        if ((this.userId == null && other.userId != null) || (this.userId != null && !this.userId.equals(other.userId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "info.hccis.admin.model.jpa.UserAccess[ userId=" + userId + " ]";
    }
    
}
