//package info.hccis.admin.util;
//
//import info.hccis.admin.util.Utility;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import org.junit.After;
//import org.junit.AfterClass;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.BeforeClass;
//import org.junit.Test;
//import static org.junit.Assert.*;
//
///**
// *
// * @author bjmaclean
// */
//public class UtilityTest {
//    
//    public UtilityTest() {
//    }
//    
//    @BeforeClass
//    public static void setUpClass() {
//        System.out.println("doing some setup if necessary...");
//    }
//    
//    @AfterClass
//    public static void tearDownClass() {
//    }
//    
//    @Before
//    public void setUp() {
//    }
//    
//    @After
//    public void tearDown() {
//    }
//
//     @Test
//     public void testGetMD5HashFor123456() {
//        String result = "";
//        try {
//            result = Utility.getMD5Hash("123456");
//        } catch (Exception ex) {
//            Logger.getLogger(UtilityTest.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        Assert.assertEquals("e10adc3949ba59abbe56e057f20f883e", result);
//        
//     }
//
//     @Test
//     public void testGetMD5HashFor11111Guess() {
//        String result = "";
//        try {
//            result = Utility.getMD5Hash("111111");
//        } catch (Exception ex) {
//            Logger.getLogger(UtilityTest.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        Assert.assertEquals("345555555555555", result);
//        
//     }
//
//}
