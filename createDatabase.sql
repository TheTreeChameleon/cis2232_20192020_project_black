CREATE DATABASE IF NOT EXISTS cis2232_fitness;
USE cis2232_fitness;

/*create a user in database*/
grant select, insert, update, delete, alter on cis2232_fitness.*
to 'cis2232_admin'@'localhost'
identified by 'Test1234';
flush privileges;

-- COMMON TABLES
DROP TABLE if exists CodeType;
CREATE TABLE CodeType (
  codeTypeId int(3) NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'This is the primary key for code types',
  englishDescription varchar(100) NOT NULL COMMENT 'English description',
  frenchDescription varchar(100) DEFAULT NULL COMMENT 'French description',
  createdDateTime datetime DEFAULT NULL,
  createdUserId varchar(20) DEFAULT NULL,
  updatedDateTime datetime DEFAULT NULL,
  updatedUserId varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This hold the code types that are available for the applicat';

--
-- Table structure for table CodeValue
--

DROP TABLE if exists CodeValue;
CREATE TABLE CodeValue (
  codeTypeId int(3) NOT NULL COMMENT 'see code_type table',
  codeValueSequence int(3) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  englishDescription varchar(100) NOT NULL COMMENT 'English description',
  englishDescriptionShort varchar(20) NOT NULL COMMENT 'English abbreviation for description',
  frenchDescription varchar(100) DEFAULT NULL COMMENT 'French description',
  frenchDescriptionShort varchar(20) DEFAULT NULL COMMENT 'French abbreviation for description',
  createdDateTime datetime DEFAULT NULL,
  createdUserId varchar(20) DEFAULT NULL,
  updatedDateTime datetime DEFAULT NULL,
  updatedUserId varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This will hold code values for the application.';

--
-- Table structure for table User
--
DROP TABLE if exists User;
CREATE TABLE User (
  userId int(3) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  username varchar(100) NOT NULL COMMENT 'Unique user name for app',
  password varchar(128) NOT NULL,
  lastName varchar(100) NOT NULL,
  firstName varchar(100) NOT NULL,
  userTypeCode int(3) NOT NULL DEFAULT '1' COMMENT 'Code type #1',
  additional1 varchar(100),
  additional2 varchar(100),
  createdDateTime varchar(20) DEFAULT NULL COMMENT 'When user was created.'
);

INSERT INTO CodeType (codeTypeId, englishDescription, frenchDescription, createdDateTime, createdUserId, updatedDateTime, updatedUserId) VALUES
(1, 'User Types', 'User Types FR', sysdate(), 'ADMIN', sysdate(), 'ADMIN'),
(2, 'Court Types', 'Court Types FR', sysdate(), 'ADMIN', sysdate(), 'ADMIN'),
(3, 'Status Types', 'User Types FR', sysdate(), 'ADMIN', sysdate(), 'ADMIN');

INSERT INTO CodeValue (codeTypeId, codeValueSequence, englishDescription, englishDescriptionShort, frenchDescription, frenchDescriptionShort, createdDateTime, createdUserId, updatedDateTime, updatedUserId) VALUES
(1, 1, 'Admin', 'Admin', 'AdminFR', 'AdminFR', sysdate(), 'ADMIN', sysdate(), 'ADMIN'),
(1, 2, 'General', 'General', 'GeneralFR', 'GeneralFR', sysdate(), 'ADMIN', sysdate(), 'ADMIN'),
(2, 3, 'Squash', 'Squash', 'SquashFR', 'SquashFR', sysdate(), 'ADMIN', sysdate(), 'ADMIN'),
(2, 4, 'Racketball', 'Racketball', 'RacketballFR', 'RacketballFR', sysdate(), 'ADMIN', sysdate(), 'ADMIN'),
(2, 5, 'Tennis', 'Tennis', 'TennisFR', 'TennisFR', sysdate(), 'ADMIN', sysdate(), 'ADMIN'),
(3, 6, 'Active', 'Active', 'Active', 'Active', sysdate(), 'ADMIN', sysdate(), 'ADMIN'),
(3, 7, 'Inactive', 'Inactive', 'Inactive', 'Inactive', sysdate(), 'ADMIN', sysdate(), 'ADMIN');

INSERT INTO User (userId, username, password, lastName, firstName, userTypeCode, additional1, additional2, createdDateTime) VALUES
(1, 'bj.maclean@gmail.com', '202cb962ac59075b964b07152d234b70', 'MacLean', 'BJ', 1, NULL, NULL, sysdate());



















-- COURT SPECIFIC TABLES



DROP TABLE if exists Court;
CREATE TABLE Court (
courtNumber int(2) NOT NULL COMMENT 'Court number ',
courtName varchar(20) NOT NULL COMMENT 'Court name',
courtType int(3) NOT NULL COMMENT 'Court Type (Code type=2)',
PRIMARY KEY (courtNumber)
);

DROP TABLE if exists CourtBooking;
CREATE TABLE CourtBooking (
 courtNumber int(2) NOT NULL COMMENT 'FK to court table',
 bookingDate varchar(8) NOT NULL COMMENT 'Booking date yyyymmdd',
 startTime varchar(4) NOT NULL COMMENT 'Start time for booking hhmm',
 memberId int(6) NOT NULL COMMENT 'FK to member table',
 memberIdOpponent int(11) NOT NULL COMMENT 'FK to member table',
 notes varchar(200) NOT NULL COMMENT 'Notes about reservation',
 createdDate varchar(8) NOT NULL COMMENT 'Date booking was made'
);

DROP TABLE if exists CourtTimes;
CREATE TABLE CourtTimes (
 startTime varchar(5) NOT NULL COMMENT 'court booking start time (hhmm)',
 endTime varchar(5) NOT NULL COMMENT 'court booking end time(hhmm)'
);

DROP TABLE if exists Member;
CREATE TABLE Member (
 userId int(6) NOT NULL COMMENT 'Unique id fk to User table',
 phoneCell varchar(10) NOT NULL COMMENT 'Cellphone number',
 phoneHome varchar(10) NOT NULL COMMENT 'Home phone number',
 phoneWork varchar(10) NOT NULL COMMENT 'Work phone number',
 address varchar(100) NOT NULL COMMENT 'Home/mailing address',
 status int(1) NOT NULL COMMENT 'Code Type 3: 1=active 0=inactive',
 membershipStartDate varchar(8) COMMENT 'Beginning of Membership',
 membershipEndDate varchar(8) COMMENT 'End of Membership'
)COMMENT='This table will hold all member information (in addition to the user table)';

DELETE FROM Court WHERE 1;
INSERT INTO Court (courtNumber, courtName, courtType) VALUES ('1', ' Court 1',1);
INSERT INTO Court (courtNumber, courtName, courtType) VALUES ('2', 'Court 2',1);
INSERT INTO Court (courtNumber, courtName, courtType) VALUES ('3', 'Court 3',1);
INSERT INTO Court (courtNumber, courtName, courtType) VALUES ('4', 'Court 4',1);
INSERT INTO Court (courtNumber, courtName, courtType) VALUES ('5', 'Court 5',2);
INSERT INTO Court (courtNumber, courtName, courtType) VALUES ('6', 'Court 6',3);
INSERT INTO Court (courtNumber, courtName, courtType) VALUES ('7', 'Court 7',3);

DELETE FROM Member WHERE 1;
INSERT INTO Member (userId,  phoneCell, phoneHome, phoneWork, address, status) VALUES 
('1', '9025661234', '9025691111', '9025669663', '104 Weymouth St.', '1');

INSERT INTO CourtTimes(startTime, endTime) VALUES ('0600','0640');
INSERT INTO CourtTimes(startTime, endTime) VALUES ('0640','0740');
INSERT INTO CourtTimes(startTime, endTime) VALUES ('0720','0800');
INSERT INTO CourtTimes(startTime, endTime) VALUES ('0800','0840');
INSERT INTO CourtTimes(startTime, endTime) VALUES ('0840','0920');
INSERT INTO CourtTimes(startTime, endTime) VALUES ('0920','1000');
INSERT INTO CourtTimes(startTime, endTime) VALUES ('1000','1040');
INSERT INTO CourtTimes(startTime, endTime) VALUES ('1040','1120');
INSERT INTO CourtTimes(startTime, endTime) VALUES ('1120','1200');
INSERT INTO CourtTimes(startTime, endTime) VALUES ('1200','1240');
INSERT INTO CourtTimes(startTime, endTime) VALUES ('1240','1320');
INSERT INTO CourtTimes(startTime, endTime) VALUES ('1320','1400');
INSERT INTO CourtTimes(startTime, endTime) VALUES ('1400','1440');
INSERT INTO CourtTimes(startTime, endTime) VALUES ('1440','1520');
INSERT INTO CourtTimes(startTime, endTime) VALUES ('1520','1600');
INSERT INTO CourtTimes(startTime, endTime) VALUES ('1600','1640');
INSERT INTO CourtTimes(startTime, endTime) VALUES ('1640','1720');
INSERT INTO CourtTimes(startTime, endTime) VALUES ('1720','1800');
INSERT INTO CourtTimes(startTime, endTime) VALUES ('1800','1840');
INSERT INTO CourtTimes(startTime, endTime) VALUES ('1840','1920');
INSERT INTO CourtTimes(startTime, endTime) VALUES ('1920','2000');
INSERT INTO CourtTimes(startTime, endTime) VALUES ('2000','2040');
INSERT INTO CourtTimes(startTime, endTime) VALUES ('2040','2120');
